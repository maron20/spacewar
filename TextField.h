#ifndef TEXTFIELD_H_INCLUDED
#define TEXTFIELD_H_INCLUDED

#include "_RECT.h"
class TextField
{
private:
    LPWSTR Text;
    HFONT Font;
    bool Visible;

    void CalcRc(POINT Pos);

public:
    _RECT TextRc;

    TextField(LPCWSTR _text, LOGFONTW _font, POINT Pos);
    TextField();
    ~TextField();

    void Show(HDC hdc, COLORREF Color, int Align);
    void SetVisibility(bool State) {Visible = State;}
};

#endif // TEXTFIELD_H_INCLUDED
