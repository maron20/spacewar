#ifndef TEXTBUTTON_H_INCLUDED
#define TEXTBUTTON_H_INCLUDED

#include <windows.h>
#include "_RECT.h"
#include "TextField.h"

class TextButton
{
private:
    //LPSTR Text;
    //_RECT rcButton;
    //HFONT Font;
    TextField Text;
    COLORREF colBase, colHover, colDraw;
    bool IsClicked, Enabled;
    void (*ClickFunc)();

    //void CalcWidth(POINT Pos);
public:
    TextButton(LPCWSTR _text, LOGFONTW _font, POINT Pos, COLORREF _colBase, COLORREF _colHover, void (*Func)());
    ~TextButton() {}
    void Show(HDC hdc);
    void GetMsg(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
    void SetEnabled(bool enabled);
};

#endif // TEXTBUTTON_H_INCLUDED
