#ifndef _RECT_H_INCLUDED
#define _RECT_H_INCLUDED

#include <windows.h>
#include "Vector.h"

class _RECT
{
private:
    Vector P[4];
    Vector Origin;

    /** Intersection **/

    double TriangSurface(const Vector &A, const Vector &B, const Vector &C) const;
    bool SegmentsIntersect(const Vector &S1, const Vector &E1, const Vector &S2, const Vector &E2) const;
    double Det(const double &a1_1, const double &a1_2, const double &a2_1, const double &a2_2) const;

    /** Priv Getters **/

    double MaxValue(bool XorY) const;
    double MinValue(bool XorY) const;

    /** Movement **/

    Vector RotatePoint(const Vector &p, const double &rot);

public:
    /** Constructors **/

    _RECT(const double &x, const double &y, const double &width, const double &height);
    _RECT(const RECT &rc);
    _RECT();

    /** Movment **/

    void Rotate(const double &angle);
    void Move(const Vector &v);

    /** Setters **/

    void Set(const double &x, const double &y, const double &width, const double &height);
    void SetPosition(const double &x, const double &y);

    /** Getters **/

    Vector GetOrigin() const;

    /** Conversion **/

    explicit operator RECT() const;

    /** Question **/

    bool IsInside(const Vector &p) const;
    bool Intersects(const _RECT &rc) const;
    bool LineIntersects(const Vector &Start, const Vector &End) const;

    /** Graphics **/
    void Fill(HDC hdc, HBRUSH brush);
};

#endif // _RECT_H_INCLUDED
