#include "_RECT.h"

/** Constructors **/

_RECT::_RECT(const double &x, const double &y, const double &width, const double &height)
{
    Set(x, y, width, height);
}

_RECT::_RECT(const RECT &rc)
{
    Set((rc.left + rc.right) / 2, (rc.top + rc.bottom) / 2, rc.right - rc.left, rc.bottom - rc.left);
}

_RECT::_RECT()
{
    Set(0, 0, 0, 0);
}

/** Movement **/

void _RECT::Rotate(const double &angle)
{
    for(int i = 0; i < 4; i++)
        P[i] = RotatePoint(P[i], angle);
}

void _RECT::Move(const Vector &v)
{
    for(int i = 0; i < 4; i++)
        P[i] += v;
    Origin += v;
}

/** Setters **/

void _RECT::Set(const double &x, const double &y, const double &width, const double &height)
{
    Origin = Vector(x, y);
    for(int i = 0; i < 2; i++)
        P[i] = Vector(x - width / 2, y + pow(-1, i + 1) * height / 2);
    for(int i = 2; i < 4; i++)
        P[i] = Vector(x + width / 2, y + pow(-1, i) * height / 2);
}

void _RECT::SetPosition(const double &x, const double &y)
{
    Vector Dist = Vector(x, y) - Origin;
    Move(Dist);
}

_RECT::operator RECT() const
{
    return {(int)floor(MinValue(true)), (int)floor(MinValue(false)), (int)ceil(MaxValue(true)), (int)ceil(MaxValue(false))};
}

/** Getters **/

Vector _RECT::GetOrigin() const
{
    return Origin;
}

/** Questions **/

bool _RECT::IsInside(const Vector &p) const
{
    double Surface = 0, ActSurface = (P[0] - P[1]).GetLength() * (P[1] - P[2]).GetLength();
    for(int i = 0; i < 4; i++)
        Surface += TriangSurface(p, P[i], P[(i + 1) % 4]);
    if(Surface > ActSurface)
        return false;
    else
        return true;
}

bool _RECT::Intersects(const _RECT &rc) const
{
    for(int i = 0; i < 4; i++)
        if(rc.LineIntersects(P[i], P[(i + 1) % 4]))
            return true;
    if(rc.IsInside(Origin) || IsInside(rc.Origin))
        return true;
    return false;
}

bool _RECT::LineIntersects(const Vector &Start, const Vector &End) const
{
    for(int i = 0; i < 4; i++)
        if(SegmentsIntersect(Start, End, P[i], P[(i + 1) % 4]))
            return true;
    return false;
}

/** Graphics **/

void _RECT::Fill(HDC hdc, HBRUSH brush)
{
    HPEN Pen = CreatePen(PS_NULL, 1, RGB(0, 0, 0));
    HPEN OldPen = (HPEN)SelectObject(hdc, Pen);
    HBRUSH OldBrush = (HBRUSH)SelectObject(hdc, brush);

    POINT tempP[4];
    for(int i = 0; i < 4; i++)
        tempP[i] = {(int)round(P[i].x), (int)round(P[i].y)};
    Polygon(hdc, tempP, 4);

    SelectObject(hdc, OldPen);
    SelectObject(hdc, OldBrush);
    DeleteObject(Pen);
    DeleteObject(OldPen);
    DeleteObject(OldBrush);
}

/** Private **/

double _RECT::TriangSurface(const Vector &A, const Vector &B, const Vector &C) const
{
    Vector AB = B - A, AC = C - A;
    return fabs(AB.x * AC.y - AB.y * AC.x) / 2;
}

bool _RECT::SegmentsIntersect(const Vector &S1, const Vector &E1, const Vector &S2, const Vector &E2) const
{
    Vector Change1 = E1 - S1, Change2 = E2 - S2;
    double d = Det(Change1.x, -Change2.x, Change1.y, -Change2.y),
           d1 = Det(S2.x - S1.x, -Change2.x, S2.y - S1.y, -Change2.y),
           d2 = Det(Change1.x, S2.x - S1.x, Change1.y, S2.y - S1.y);

    if(d != 0 && d1 / d >= 0 && d1 / d <= 1 && d2 / d >= 0 && d2 / d <= 1)
        return true;
    return false;
}

double _RECT::Det(const double &a1_1, const double &a1_2, const double &a2_1, const double &a2_2) const
{
    return a1_1 * a2_2 - a1_2 * a2_1;
}

double _RECT::MaxValue(bool XorY) const
{
    double Max = (XorY) ? P[0].x : P[0].y;
    if(XorY)
        for(int i = 1; i < 4; i++)
            Max = (Max > P[i].x) ? Max : P[i].x;
    else
        for(int i = 1; i < 4; i++)
            Max = (Max > P[i].y) ? Max : P[i].y;
    return Max;
}

double _RECT::MinValue(bool XorY) const
{
    double Min = (XorY) ? P[0].x : P[0].y;
    if(XorY)
        for(int i = 1; i < 4; i++)
            Min = (Min < P[i].x) ? Min : P[i].x;
    else
        for(int i = 1; i < 4; i++)
            Min = (Min < P[i].y) ? Min : P[i].y;
    return Min;
}

Vector _RECT::RotatePoint(const Vector &p, const double &rot)
{
    Vector temp = p - Origin;
    Vector _p(temp.x * cos(rot) - temp.y * sin(rot), temp.x * sin(rot) + temp.y * cos(rot));
    return _p + Origin;
}
