#ifndef BULLET_H_INCLUDED
#define BULLET_H_INCLUDED

#include "Vector.h"

class Bullet
{
private:
    HWND hwnd;
    Vector Pos;
    Vector PrevPos;
    Vector Speed;
    RECT Bounds;
public:
    unsigned Number;

    Bullet(const Vector &_Pos, const Vector &ShipSpeed, double Direction, int Num, HWND _hwnd);
    ~Bullet();
    void Move();
    void Draw(HDC hdc);
    Vector GetPos() const {return Pos;}
    Vector GetPrevPos() const {return PrevPos;}
};

#endif // BULLET_H_INCLUDED
