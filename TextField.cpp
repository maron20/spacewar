#include "TextField.h"
#include <cwchar>

TextField::TextField(LPCWSTR _text, LOGFONTW _font, POINT pos)
{
    Text = new WCHAR[wcslen(_text)];
    wcscpy(Text, _text);
    Font = CreateFontIndirectW(&_font);
    CalcRc(pos);
    Visible = true;
}
TextField::TextField()
{
    Text = new WCHAR[1];
    wcscpy(Text, L"\0");
    CalcRc({0, 0});
    Visible = true;
    Font = NULL;
}

TextField::~TextField()
{
    delete [] Text;
    DeleteObject(Font);
}

void TextField::Show(HDC hdc, COLORREF Color, int Align)
{
    if(Visible)
    {
        COLORREF OldCol = SetTextColor(hdc, Color);
        HFONT OldFont = (HFONT)SelectObject(hdc, Font);
        int OldMode = SetBkMode(hdc, TRANSPARENT);
        RECT temp = (RECT)TextRc;

        DrawTextW(hdc, Text, wcslen(Text), &temp, Align | DT_VCENTER);

        SetTextColor(hdc, OldCol);
        SetBkMode(hdc, OldMode);
        SelectObject(hdc, OldFont);
        DeleteObject(OldFont);
    }
}

void TextField::CalcRc(POINT Pos)
{
    WCHAR *Start, *End;
    Start = End = Text;
    SIZE Size = {0, 0}, LineSize;

    HDC hdc = GetDC(NULL);
    HFONT OldFont = (HFONT)SelectObject(hdc, Font);
    while(true)
    {
        while(*End != '\n' && *End != '\0')
            End += 1;
        GetTextExtentPoint32W(hdc, Start, End - Start, &LineSize);
        Size.cy += LineSize.cy;
        if(LineSize.cx > Size.cx)
            Size.cx = LineSize.cx;
        if(*End == '\0')
            break;
        Start = End = End + 1;
    }
    TextRc = _RECT(Pos.x, Pos.y, Size.cx, Size.cy);

    SelectObject(hdc, OldFont);
    DeleteObject(OldFont);
}
