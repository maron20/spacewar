#if defined(UNICODE) && !defined(_UNICODE)
#define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
#define UNICODE
#endif

#include <tchar.h>
#include <windows.h>
#include "Sprite.h"
#include "TextButton.h"
#include <cmath>
#include <iostream>

const int VK_A = 0x41;
const int VK_S = 0x53;
const int VK_W = 0x57;
const int VK_D = 0x44;
const int PREVKEYSTATE = 0x40000000;

const WORD ANIMATION_TIMER = 1;
enum M {MENU, GAME, HELP};

const double RotationSpeed = 0.02;
const double Acceleration = 0.003;
const double Gravity = 70;

const Vector StartPos1(100, 100);
const Vector StartPos2(400, 400);

/**  Declare Windows procedure  **/

LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/** Graphics **/

void Draw(void (*fDraw)(HDC));
void DrawGame(HDC);
void DrawMenu(HDC);
void DrawHelp(HDC);
void DrawStar(HDC);

/** Game Logic **/

void Init(HWND);
void AddGravity(Ship&);
bool CheckPosition(Ship&);

/** Menu **/

void ToMenu();
void StartFunc();
void QuitFunc();
void ReturnFunc();
void HelpFunc();
void EnableMenu(bool);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("SpaceWar");

HWND g_hwnd;
RECT rcWindow;
HDC hdcBuffor;
HBITMAP hbmBuff, hbmOldBuff;

LOGFONTW HelpFont;

TextButton *StartBut, *QuitBut, *HelpBut, *ReturnBut;
TextField *HelpText;
std::vector<Bullet*> BullList;
M MODE = MENU;

_RECT *Star;
Ship **Player = new Ship*[2];
bool DoRotate[2] = {false, false}, DoMove[2] = {false, false};
int BulletNum = 100;

int WINAPI WinMain (HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
               0,                   /* Extended possibilites for variation */
               szClassName,         /* Classname */
               _T("Space War"),       /* Title Text */
               WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME, /* default window */
               CW_USEDEFAULT,       /* Windows decides the position */
               CW_USEDEFAULT,       /* where the window ends up on the screen */
               500,                 /* The programs width */
               500,                 /* and height in pixels */
               HWND_DESKTOP,        /* The window is a child-window to desktop */
               NULL,                /* No menu */
               hThisInstance,       /* Program Instance handler */
               NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);
    Init(hwnd);
    Draw(DrawMenu);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}
/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    QuitBut->GetMsg(hwnd, message, wParam, lParam);
    StartBut->GetMsg(hwnd, message, wParam, lParam);
    HelpBut->GetMsg(hwnd, message, wParam, lParam);
    ReturnBut->GetMsg(hwnd, message, wParam, lParam);
    switch (message)                  /* handle the messages */
    {
    case WM_TIMER:
    {
        if(MODE == GAME)
        {
            if(wParam == ANIMATION_TIMER)
            {
                for(int i = 0; i < 2; i++)
                {
                    if(DoRotate[i])
                        Player[i]->Rotate();
                    if(DoMove[i])
                        Player[i]->Accelerate();
                    for(std::vector<Bullet*>::iterator j = BullList.begin(); j != BullList.end(); j++)
                        (*j)->Move();
                    AddGravity(*Player[i]);
                    if(!CheckPosition(*Player[i]))
                    {
                        ToMenu();
                        break;
                    }
                }
                Draw(DrawGame);
            }

            for(std::vector<Bullet*>::iterator i = BullList.begin(); i != BullList.end(); i++)
                if((*i)->Number == wParam)
                {
                    delete *i;
                    BullList.erase(i);
                    break;
                }
        }
        else if(MODE == MENU)
            Draw(DrawMenu);
        else
            Draw(DrawHelp);
        break;
    }
    case WM_KEYDOWN:
    {
        if(MODE == GAME)
            switch(wParam)
            {
            case VK_LEFT:
                DoRotate[0] = true;
                Player[0]->SetRotSpeed(RotationSpeed);
                break;
            case VK_RIGHT:
                DoRotate[0] = true;
                Player[0]->SetRotSpeed(-RotationSpeed);
                break;
            case VK_DOWN:
                DoMove[0] = true;
                Player[0]->SetAcceleration(Acceleration);
                break;
            case VK_UP:
                if(!(lParam & PREVKEYSTATE))
                    Player[0]->Shoot(BullList, hwnd, BulletNum++);
                break;
            case VK_A:
                DoRotate[1] = true;
                Player[1]->SetRotSpeed(RotationSpeed);
                break;
            case VK_D:
                DoRotate[1] = true;
                Player[1]->SetRotSpeed(-RotationSpeed);
                break;
            case VK_S:
                DoMove[1] = true;
                Player[1]->SetAcceleration(Acceleration);
                break;
            case VK_W:
                if(!(lParam & PREVKEYSTATE))
                    Player[1]->Shoot(BullList, hwnd, BulletNum++);
                break;
            case VK_ESCAPE:
                ToMenu();
                break;
            }
        break;
    }
    case WM_KEYUP:
    {
        if(MODE == GAME)
            switch(wParam)
            {
            case VK_LEFT:
            case VK_RIGHT:
                DoRotate[0] = false;
                Player[0]->SetRotSpeed(0);
                break;
            case VK_DOWN:
                DoMove[0] = false;
                Player[0]->SetAcceleration(0);
                break;
            case VK_A:
            case VK_D:
                DoRotate[1] = false;
                Player[1]->SetRotSpeed(0);
                break;
            case VK_S:
                DoMove[1] = false;
                Player[1]->SetAcceleration(0);
                break;
            }
        break;
    }
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        KillTimer(g_hwnd, ANIMATION_TIMER);
        delete StartBut;
        delete QuitBut;
        delete ReturnBut;
        delete HelpBut;
        delete HelpText;
        delete Star;
        delete Player[0];
        delete Player[1];
        delete[] Player;
        PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
        break;
    default:                      /* for messages that we don't deal with */
        return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

/** Graphics **/

void DrawMenu(HDC hdc)
{
    StartBut->Show(hdc);
    HelpBut->Show(hdc);
    QuitBut->Show(hdc);


}

void DrawGame(HDC hdc)
{
    DrawStar(hdc);
    Player[0]->Draw(hdc);
    Player[1]->Draw(hdc);

    for(std::vector<Bullet*>::iterator i = BullList.begin(); i != BullList.end(); i++)
        (*i)->Draw(hdc);
}

void DrawHelp(HDC hdc)
{
    HelpText->Show(hdc, RGB(255, 255, 255), DT_LEFT);
    ReturnBut->Show(hdc);
}

void Draw(void (*fDraw)(HDC))
{
    HBRUSH BgBrush = CreateSolidBrush(RGB(0, 0, 0));
    FillRect(hdcBuffor, &rcWindow, BgBrush);
    DeleteObject(BgBrush);

    fDraw(hdcBuffor);

    HDC hdc = GetDC(g_hwnd);
    BitBlt(hdc, 0, 0, rcWindow.right, rcWindow.bottom, hdcBuffor, 0, 0, SRCCOPY);
    ReleaseDC(g_hwnd, hdc);
}

void DrawStar(HDC hdc)
{
    HPEN Pen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
    HPEN OldPen = (HPEN)SelectObject(hdc, Pen);
    int x = rand() % 21 - 10, y = rand() % 21 - 10;

    MoveToEx(hdc, x + Star->GetOrigin().x, y + Star->GetOrigin().y, NULL);
    LineTo(hdc, -x + Star->GetOrigin().x, -y + Star->GetOrigin().y);

    SelectObject(hdc, OldPen);
    DeleteObject(Pen);
    DeleteObject(OldPen);
}

/** Game Logic **/

void Init(HWND hwnd)
{
    g_hwnd = hwnd;
    if(SetTimer(g_hwnd, ANIMATION_TIMER, 1, NULL) == 0)
        MessageBox(g_hwnd, "Timer not created!", "Damn!", MB_ICONSTOP);

    GetClientRect(g_hwnd, &rcWindow);
    Star = new _RECT(rcWindow.right / 2, rcWindow.bottom / 2, 3, 3);
    Player[0] = new Ship(StartPos1.x, StartPos1.y, "Ship.bmp", RGB(255, 0, 255));
    Player[1] = new Ship(StartPos2.x, StartPos2.y, "Ship2.bmp", RGB(255, 0, 255));

    HDC hdc = GetDC(g_hwnd);
    hdcBuffor = CreateCompatibleDC(hdc);
    hbmBuff = CreateCompatibleBitmap(hdc, rcWindow.right, rcWindow.bottom);
    hbmOldBuff = (HBITMAP)SelectObject(hdcBuffor, hbmBuff);
    ReleaseDC(g_hwnd, hdc);

    LOGFONTW TextFont;
    TextFont.lfHeight = 50;
    TextFont.lfEscapement = 0;
    TextFont.lfOrientation  = 0;
    TextFont.lfWidth = 0;
    TextFont.lfItalic = FALSE;
    TextFont.lfUnderline = FALSE;
    TextFont.lfStrikeOut = FALSE;
    TextFont.lfQuality = ANTIALIASED_QUALITY;
    TextFont.lfWeight = FW_DONTCARE;
    TextFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    wcscpy(TextFont.lfFaceName, L"Arial");

    HelpFont.lfHeight = 40;
    HelpFont.lfEscapement = 0;
    HelpFont.lfOrientation  = 0;
    HelpFont.lfWidth = 0;
    HelpFont.lfItalic = FALSE;
    HelpFont.lfUnderline = FALSE;
    HelpFont.lfStrikeOut = FALSE;
    HelpFont.lfQuality = ANTIALIASED_QUALITY;
    HelpFont.lfWeight = FW_DONTCARE;
    HelpFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    wcscpy(HelpFont.lfFaceName, L"Arial");


    StartBut = new TextButton(L"Start", TextFont, {rcWindow.right / 2, rcWindow.bottom / 2 - 100}, RGB(255, 255, 255), RGB(150, 150, 150), StartFunc);
    QuitBut = new TextButton(L"Quit", TextFont, {rcWindow.right / 2, rcWindow.bottom / 2 + 100}, RGB(255, 255, 255), RGB(150, 150, 150), QuitFunc);
    HelpBut = new TextButton(L"Help", TextFont, {rcWindow.right / 2, rcWindow.bottom / 2}, RGB(255, 255, 255), RGB(150, 150, 150), HelpFunc);
    ReturnBut = new TextButton(L"Return", TextFont, {rcWindow.right / 2, rcWindow.bottom  - 75}, RGB(255, 255, 255), RGB(150, 150, 150), ReturnFunc);
    ReturnBut->SetEnabled(false);

    HelpText = new TextField(L"            PL1   PL2\nRotate   A/D   ◀/▶\nMove      S      ▼ \nShoot     W     ▲", HelpFont, {rcWindow.right / 2, rcWindow.bottom / 2 - 50});
    HelpText->SetVisibility(false);
}

void AddGravity(Ship &Pl)
{
    Vector DistToCentre = Star->GetOrigin() - Pl.GetPosition();
    double Angle = DistToCentre.GetAngle();
    Vector G = Vector(cos(Angle), sin(Angle)) * Gravity / pow(DistToCentre.GetLength(), 2);
    Pl.AddAcceleration(G);
}

bool CheckPosition(Ship &Pl)
{
    if(Pl.GetPosition().x > rcWindow.right)
        Pl.SetX(rcWindow.left);
    else if(Pl.GetPosition().x < rcWindow.left)
        Pl.SetX(rcWindow.right);

    if(Pl.GetPosition().y > rcWindow.bottom)
        Pl.SetY(rcWindow.top);
    else if(Pl.GetPosition().y < rcWindow.top)
        Pl.SetY(rcWindow.bottom);

    if(Star->Intersects(Pl.GetRect()))
        return false;

    for(std::vector<Bullet*>::iterator i = BullList.begin(); i != BullList.end(); i++)
        if(Pl.LineIntersects((*i)->GetPos(), (*i)->GetPrevPos()))
            return false;
    return true;
}

/** Menu **/

void StartFunc()
{
    Player[0]->Reset(StartPos1);
    Player[1]->Reset(StartPos2);
    MODE = GAME;
    EnableMenu(false);
}

void QuitFunc()
{
    SendMessage(g_hwnd, WM_CLOSE, 0, 0);
}

void ReturnFunc()
{
    MODE = MENU;
    EnableMenu(true);
    ReturnBut->SetEnabled(false);
}

void HelpFunc()
{
    MODE = HELP;
    EnableMenu(false);
    ReturnBut->SetEnabled(true);
    HelpText->SetVisibility(true);
}

void ToMenu()
{
    Player[0]->SetEnabled(false);
    Player[1]->SetEnabled(false);
    EnableMenu(true);

    for(std::vector<Bullet*>::iterator i = BullList.begin(); i != BullList.end(); i++)
        delete (*i);
    BullList.clear();
    BulletNum = 100;
    MODE = MENU;
}

void EnableMenu(bool State)
{
    StartBut->SetEnabled(State);
    HelpBut->SetEnabled(State);
    QuitBut->SetEnabled(State);
}
