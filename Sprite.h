#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <tchar.h>
#include <windows.h>
#include <iostream>
#include "Vector.h"
#include "_RECT.h"
#include "Bullet.h"
#include <vector>
#include "Bitmap.h"

class Ship
{
private:
    static constexpr double MaxSpeed = 3;
    _RECT rcBounds;
    SIZE Size;
    Vector Position, Speed;
    double Rotation, RotSpeed, Acceleration;
    bool enabled;
    Bitmap *bmp;

    void UpdateRc();
    void Move();
    void Drag();
public:

    /** Basic **/

    Ship(const int &x, const int &y, LPCSTR FileName, COLORREF TransparentCol);
    ~Ship();
    void Draw(HDC hdc);

    /** Movement **/

    void Rotate();
    void Accelerate();
    void AddAcceleration(const Vector &a);
    void Shoot(std::vector<Bullet*> &Bullets, HWND hwnd, int Number);

    /** Getters **/

    Vector GetPosition() const {return Position;}
    _RECT GetRect() const {return rcBounds;}
    Vector GetSpeed() const {return Speed;}
    double GetRotation() const {return Rotation;}
    Vector GetShootPos() const;

    /** Setters **/

    void SetX(const int &x);
    void SetY(const int &y);
    void Reset(const Vector &p);
    void SetEnabled(const bool &State);
    void SetAcceleration(const double &a);
    void SetRotSpeed(const double &rot);

    /** Questions **/

    bool IsInside(const Vector &p) const {return rcBounds.IsInside(p);}
    bool LineIntersects(const Vector &p1, const Vector &p2) const {return rcBounds.LineIntersects(p1, p2);}
};

#endif // SPRITE_H_INCLUDED
