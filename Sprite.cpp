#include "Sprite.h"
#include <cmath>
#include <algorithm>

/** Basic **/

Ship::Ship(const int &x, const int &y, LPCSTR FileName, COLORREF TransparentCol)
{
    Position = Vector(x, y);
    bmp =  new Bitmap(FileName, TransparentCol);
    Size = bmp->GetOrigSize();
    UpdateRc();
    Speed = Vector();
    Rotation = RotSpeed = Acceleration = 0;
    enabled = true;
}

Ship::~Ship()
{
    delete bmp;
}

void Ship::Draw(HDC hdc)
{
    if(enabled)
    {
        HPEN Pen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
        HBRUSH Brush = CreateSolidBrush(RGB(255, 255, 255));
        HPEN OldPen = (HPEN)SelectObject(hdc, Pen);

        Move();
        bmp->Show(hdc, Position, Rotation);

        SelectObject(hdc, OldPen);
        DeleteObject(Pen);
        DeleteObject(OldPen);
        DeleteObject(Brush);
    }
}

/** Movement **/

void Ship::Rotate()
{
    if(enabled)
    {
        Rotation -= RotSpeed;
        rcBounds.Rotate(-RotSpeed);
    }
}

void Ship::Accelerate()
{
    AddAcceleration(Vector(cos(Rotation), sin(Rotation)) * Acceleration);
}

void Ship::AddAcceleration(const Vector &a)
{
    double temp = MaxSpeed;
    if(enabled)
        Speed += a;
    if(Speed.GetLength() > MaxSpeed)
        Speed = (Speed / Speed.GetLength()) * temp;
}

void Ship::Shoot(std::vector<Bullet*> &Bullets, HWND hwnd, int Number)
{
    Bullet *temp = new Bullet(Position + Vector(cos(Rotation), sin(Rotation)) * (Size.cx / 1.9), Speed, Rotation, Number, hwnd);
    Bullets.push_back(temp);
}

/** Getters **/

Vector Ship::GetShootPos() const
{
    return Position + Vector(cos(Rotation), sin(Rotation)) * (Size.cx / 2);
}

/** Setters **/

void Ship::SetX(const int &x)
{
    if(enabled)
    {
        double ChangeX = x - Position.x;
        rcBounds.Move(Vector(ChangeX, 0));
        Position.x = x;
    }
}

void Ship::SetY(const int &y)
{
    if(enabled)
    {
        double ChangeY = y - Position.y;
        rcBounds.Move(Vector(0, ChangeY));
        Position.y = y;
    }

}

void Ship::Reset(const Vector &p)
{
    Rotation = 0;
    Acceleration = 0;
    RotSpeed = 0;
    enabled = true;
    Speed = Vector();
    Position = p;
    UpdateRc();
}

void Ship::SetEnabled(const bool &State)
{
    enabled = State;
}

void Ship::SetAcceleration(const double &a)
{
    Acceleration = a;
}

void Ship::SetRotSpeed(const double &rot)
{
    RotSpeed = rot;
}

/** Private **/

void Ship::Move()
{
    Position += Speed;
    rcBounds.Move(Speed);
}

void Ship::Drag()
{
    double Angle = Speed.GetAngle();
    Speed += Vector(cos(Angle), sin(Angle)) * 0.01 * Speed.GetLength();
}

void Ship::UpdateRc()
{
    rcBounds = _RECT(Position.x, Position.y, Size.cx, Size.cy);
}
