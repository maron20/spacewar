#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <windows.h>
#include <cmath>

class Vector
{
public:
    double x, y;

    /** Basic **/

    Vector(const double &_x, const double &_y);
    Vector(const Vector &v);
    Vector(const POINT &p);
    Vector();

    /** Getters **/

    double GetAngle();
    double GetLength();

    /** Operators **/

    Vector operator+(const Vector &v) const;
    Vector operator-(const Vector &v) const;
    Vector operator*(const double &a) const;
    Vector operator/(const double &a) const;
    void operator+=(const Vector &v);
    void operator-=(const Vector &v);
    void operator*=(const double &a);
    void operator/=(const double &a);
    friend Vector operator*(const double &a, const Vector &v);

    /** Conversion **/

    operator POINT() const;
    operator SIZE() const;
};

#endif // VECTOR_H_INCLUDED
