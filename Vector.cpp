#include "Vector.h"


/** Basic **/

Vector::Vector(const double &_x, const double &_y)
{
    x = _x;
    y = _y;
}

Vector::Vector(const Vector &v)
{
    x = v.x;
    y = v.y;
}

Vector::Vector(const POINT &p)
{
    x = p.x;
    y = p.y;
}

Vector::Vector()
{
    x = 0;
    y = 0;
}

/** Getters **/

double Vector::GetAngle()
{
    if(x == 0 && y == 0)
        return 0;
    else
        return atan2(y, x);
}
double Vector::GetLength()
{
    return sqrt(y * y + x * x);
}

/** Operators **/

Vector Vector::operator+(const Vector &v) const
{
    return Vector(x + v.x, y + v.y);
}

Vector Vector::operator-(const Vector &v) const
{
    return Vector(x - v.x, y - v.y);
}

Vector Vector::operator*(const double &a) const
{
    return Vector(x * a, y * a);
}

Vector Vector::operator/(const double &a) const
{
    return Vector(x / a, y / a);
}

void Vector::operator+=(const Vector &v)
{
    (*this) = (*this) + v;
}

void Vector::operator-=(const Vector &v)
{
    (*this) = (*this) - v;
}

void Vector::operator*=(const double &a)
{
    (*this) = (*this) * a;
}

void Vector::operator/=(const double &a)
{
    (*this) = (*this) / a;
}

Vector operator*(const double &a, const Vector &v)
{
    return v * a;
}

/** Conversion **/

Vector::operator POINT() const
{
    return {(int)round(x), (int)round(y)};
}

Vector::operator SIZE() const
{
    return {(int)ceil(x), (int)ceil(y)};
}
