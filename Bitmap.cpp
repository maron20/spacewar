#include "Bitmap.h"
#include <windows.h>

const double M_PI = 3.14159265358979323846;

Bitmap::Bitmap(LPCSTR FileName, COLORREF _TransparentCol)
{
    hbmOrig = (HBITMAP)LoadImage(NULL, FileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    hdcMem = CreateCompatibleDC(NULL);
    TransparentCol = _TransparentCol;
    hbmMask = CreateBitmapMask(hbmOrig);
    hbmDelete = (HBITMAP)SelectObject(hdcMem, hbmOrig);
}

Bitmap::Bitmap()
{
    hbmOrig = hbmDelete = hbmMask = NULL;
    hdcMem = NULL;
}

Bitmap::~Bitmap()
{
    SelectObject(hdcMem, hbmDelete);
    DeleteDC(hdcMem);
    DeleteObject(hbmOrig);
    DeleteObject(hbmDelete);
}

HBITMAP Bitmap::CreateBitmapMask(HBITMAP hbm)
{
   HDC hdcMem, hdcMem2;
    HBITMAP hbmMask, hbmOld, hbmOld2;
    BITMAP bm;

    GetObject( hbm, sizeof( BITMAP ), & bm );
    hbmMask = CreateBitmap( bm.bmWidth, bm.bmHeight, 1, 1, NULL );

    hdcMem = CreateCompatibleDC( NULL );
    hdcMem2 = CreateCompatibleDC( NULL );

    hbmOld =( HBITMAP ) SelectObject( hdcMem, hbm );
    hbmOld2 =( HBITMAP ) SelectObject( hdcMem2, hbmMask );

    SetBkColor( hdcMem, TransparentCol );

    BitBlt( hdcMem2, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY );
    BitBlt(hdcMem2, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem2, 0, 0, NOTSRCCOPY);

    SelectObject( hdcMem, hbmOld );
    SelectObject( hdcMem2, hbmOld2 );
    DeleteObject(hbmOld2);
    DeleteObject(hbmOld);
    DeleteDC( hdcMem );
    DeleteDC( hdcMem2 );

    return hbmMask;
}

void Bitmap::Show(HDC hdc, Vector Pos, double Rotation)
{
    Vector *Points[3];

    BITMAP OrigBm;
    GetObject(hbmOrig, sizeof(OrigBm), &OrigBm);

    double DiamAngle = Vector(OrigBm.bmWidth, OrigBm.bmHeight).GetAngle(),
           DiamLength = Vector(OrigBm.bmWidth, OrigBm.bmHeight).GetLength() / 2;

    Points[0] = new Vector(cos(M_PI - DiamAngle + Rotation), sin(M_PI - DiamAngle + Rotation));
    Points[1] = new Vector(cos(DiamAngle + Rotation), sin(DiamAngle + Rotation));
    Points[2] = new Vector(cos(M_PI + DiamAngle + Rotation), sin(M_PI + DiamAngle + Rotation));
    for(int i = 0; i < 3; i++)
        *(Points[i]) = (*(Points[i]) * DiamLength) + Pos;

    POINT P[3] = {(POINT)*(Points[0]), (POINT)*(Points[1]), (POINT)*(Points[2])};

    PlgBlt(hdc, P, hdcMem, 0, 0, OrigBm.bmWidth, OrigBm.bmHeight, hbmMask, 0, 0);

    for(int i = 0; i < 3; i++)
        delete Points[i];
}

SIZE Bitmap::GetOrigSize() const
{
    BITMAP bm;
    GetObject(hbmOrig, sizeof(bm), &bm);
    return {bm.bmWidth, bm.bmHeight};
}
