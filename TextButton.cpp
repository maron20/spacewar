#include "TextButton.h"

TextButton::TextButton(LPCWSTR _text, LOGFONTW _font, POINT Pos, COLORREF _colBase, COLORREF _colHover, void (*Func)()) : Text(_text,_font, Pos)
{
    colBase = colDraw = _colBase;
    colHover = _colHover;
    ClickFunc = Func;
    IsClicked = false;
    Enabled = true;
}

void TextButton::Show(HDC hdc)
{
    if(Enabled)
        Text.Show(hdc, colDraw, DT_CENTER);
}

void TextButton::GetMsg(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
    case WM_LBUTTONDOWN:
        if(Enabled)
        {
            Vector Pos(LOWORD(lParam), HIWORD(lParam));
            if(Text.TextRc.IsInside(Pos))
                IsClicked = true;
        }
        break;
    case WM_LBUTTONUP:
        if(Enabled)
        {
            Vector Pos(LOWORD(lParam), HIWORD(lParam));
            if(Text.TextRc.IsInside(Pos) && IsClicked)
            {
                IsClicked = false;
                ClickFunc();
            }
        }
        break;
    case WM_MOUSEMOVE:
        if(Enabled)
        {
            Vector Pos(LOWORD(lParam), HIWORD(lParam));
            if(Text.TextRc.IsInside(Pos))
                colDraw = colHover;
            else
                colDraw = colBase;
        }
        break;
    }
}


void TextButton::SetEnabled(bool enabled)
{
    Enabled = enabled;
    colDraw = colBase;
}
