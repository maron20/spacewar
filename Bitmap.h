#ifndef BITMAP_H_INCLUDED
#define BITMAP_H_INCLUDED

#include "Vector.h"

class Bitmap
{
private:
    HBITMAP hbmOrig, hbmDelete, hbmMask;
    HDC hdcMem;
    COLORREF TransparentCol;

    HBITMAP CreateBitmapMask(HBITMAP hbm);
public:
    Bitmap(LPCSTR FileName, COLORREF _TransparentCol);
    Bitmap();
    ~Bitmap();
    void Show(HDC hdc, Vector Pos, double Rotation);

    SIZE GetOrigSize() const;
};

#endif // BITMAP_H_INCLUDED
