#include "Bullet.h"
#include <iostream>

Bullet::Bullet(const Vector &_Pos, const Vector &ShipSpeed, double Direction, int Num, HWND _hwnd)
{
    hwnd = _hwnd;
    Pos = PrevPos = _Pos;
    Speed = ShipSpeed * 0.5 + Vector(cos(Direction), sin(Direction)) * 2;
    Number = Num;
    GetClientRect(_hwnd, &Bounds);
    if(SetTimer(hwnd, Number, 1500, NULL) == 0)
        MessageBox(hwnd, "Timer not created!", "Damn!", MB_ICONSTOP);
}

Bullet::~Bullet()
{
    KillTimer(hwnd, Number);
}

void Bullet::Move()
{
    PrevPos = Pos;
    Pos += Speed;

    if(Pos.x > Bounds.right)
        Pos.x = Bounds.left;
    else if(Pos.x < Bounds.left)
        Pos.x = Bounds.right;

    if(Pos.y > Bounds.bottom)
        Pos.y = Bounds.top;
    else if(Pos.y < Bounds.top)
        Pos.y = Bounds.bottom;
}

void Bullet::Draw(HDC hdc)
{
    HPEN Pen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
    HPEN OldPen = (HPEN)SelectObject(hdc, Pen);

    Vector UnitV = Speed / Speed.GetLength();

    MoveToEx(hdc, round(Pos.x + UnitV.x), round(Pos.y + UnitV.y), NULL);
    LineTo(hdc, round(Pos.x - UnitV.x), round(Pos.y - UnitV.x));

    SelectObject(hdc, OldPen);
    DeleteObject(Pen);
    DeleteObject(OldPen);
}
